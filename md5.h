/*
 * Create an MD5 hex digest from a string.
 * 33-byte string is malloc'd by this function. The caller must free it.
 * When compiling, link to:  -l ssl -l crypto
 */
char *md5(const char *str, int length);

/*
 * Create an MD5 digest from a string.
 * Returns a pointer to a 16-byte array containing the digest.
 * The array is internal to the md5digest function; do not free it;
 * it will be re-used on the next call.
 */
unsigned char *md5digest(const char *str, int length);


/*
 * Convert a hex string into a digest.
 * Returns a pointer to an internal 16-byte array. Do not free it.
 */
unsigned char *hex2digest(char *hex);


/*
 * Convert a digest into a string of hex digits.
 * Returns a pointer to an internal string. Do not free it.
 */
char *digest2hex(unsigned char *digest);
