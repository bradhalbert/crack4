#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "entry.h"
#include "md5.h"

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Usage: %s rainbow_file\n", argv[0]);
        exit(1);
    }
    
    FILE *f = fopen(argv[1], "r");
    if (!f)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    struct entry blank = { .pass = "" };
    struct entry e;
    int count = 0;
    
    while (fread(&e, sizeof(struct entry), 1, f))
    {
        if (memcmp(&e, &blank, sizeof(struct entry)) != 0)
        {
            int idx = e.hash[0]*65536+e.hash[1]*256+e.hash[2];
            printf("%s 0x%06x (%d): %s %s\n",
                   (count != idx ? "*" : " "), 
                   count, 
                   count, 
                   digest2hex(e.hash), 
                   e.pass);
        }
        count++;
    }
    fclose(f);
}
